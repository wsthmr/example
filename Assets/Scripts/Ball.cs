﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour
{
    #region Parameters

    [SerializeField]
    private float speed = 5;

    #endregion Parameters

    #region State

    private Vector2 direction;
    private SpriteRenderer renderrer;

    #endregion State

    #region Methods

    private void Movement(Vector3 direction)
    {
        transform.position = Vector3.MoveTowards(transform.position, direction, speed * Time.deltaTime);
    }

    #endregion Methods

    #region Unity
    private void Awake()
    {
        renderrer = GetComponent<SpriteRenderer>();
    }
    private void Update()
    {
        Movement(new Vector2(transform.position.x + direction.x, transform.position.y));
    }

    private void OnCollisionEnter2D(Collision2D col)
    {
        direction = Vector2.Reflect(direction, col.contacts[0].normal);
    }
    #endregion Untity
}
