﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character : MonoBehaviour
{
    #region Parameters
    [SerializeField]
    private float speed = 2.5f;
    [SerializeField]
    private float jump = 10.5f;
    [SerializeField]
    private int health = 100;
    [SerializeField]
    private GameObject respawnMark;

    #endregion Parameters

    #region State
    private Rigidbody2D rigidBody;

    private BoxCollider2D boxCollider;

    private SpriteRenderer renderrer;

    private Animator animator;

    #endregion State

    #region Methods

    private void Movement(Vector2 direction)
    {
        transform.position = Vector2.MoveTowards(transform.position, direction, speed * Time.deltaTime);
    }

    private void Respawn()
    {
        transform.position = respawnMark.transform.position;
    }

    private void Jump()
    {
        rigidBody.AddForce(Vector2.up * jump, ForceMode2D.Impulse);
    }

    #endregion Methods

    #region Unity

    private void Awake()
    {
        rigidBody = GetComponent<Rigidbody2D>();

        boxCollider = GetComponent<BoxCollider2D>();

        renderrer = GetComponentInChildren<SpriteRenderer>();

        animator = GetComponent<Animator>();
    }

    private void Start()
    {
        Respawn();
    }

    private void Update()
    {
        animator.SetBool("Is Move", Input.GetButton("Horizontal"));
        if (Input.GetButton("Horizontal")) renderrer.flipX = Input.GetAxis("Horizontal") < 0;

        if (Input.GetKeyDown(KeyCode.Space) && Physics2D.OverlapCircleAll(new Vector2(transform.position.x, transform.transform.position.y - 1f), 0.2f).Length > 1)
        {
            Jump();
        }

        if (Input.GetButton("Horizontal"))
        {
            Vector2 direction = new Vector2(transform.position.x + Input.GetAxis("Horizontal"), transform.position.y + 0);
            Movement(direction);
        }
    }

    private void OnCollisionEnter2D(Collision2D col)
    {
        Eminem eminem = col.gameObject.GetComponent<Eminem>();
        if (eminem != null)
        {
            Respawn();
        }

        Spike spike = col.gameObject.GetComponent<Spike>();
        if (spike != null)
        {
            Respawn();
        }

        Ball ball = col.gameObject.GetComponent<Ball>();
        if (ball != null)
        {
            Respawn();
        }

        //SpikeFall spikefall = col.gameObject.GetComponent<SpikeFall>();
        //if (spikefall != null)
        //{
        //  Respawn();
        //}
    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.name == "EndLVL" || col.gameObject.name == "Resp") Respawn();
    }

    #endregion Unity
}
