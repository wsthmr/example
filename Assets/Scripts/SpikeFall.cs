﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpikeFall : MonoBehaviour
{
    #region Parameters

    [SerializeField]
    private float speed = 10;

    #endregion Parameters




    #region State
    private Rigidbody2D rigidBody;
    private SpriteRenderer renderrer;

    #endregion State




    #region  Methods
    private void Fall()
    {

    }

    #endregion Methods




    #region  Unity
    private void Awake()
    {
        rigidBody = GetComponent<Rigidbody2D>();
        renderrer = GetComponent<SpriteRenderer>();
    }
    private void Update()
    {

    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.GetComponent<Character>() != null)
        {
            rigidBody.bodyType = RigidbodyType2D.Dynamic;
        }
    }

    #endregion Unity
}
