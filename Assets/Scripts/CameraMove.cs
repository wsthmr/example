﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMove : MonoBehaviour
{
    #region Parameters

    [SerializeField]
    private GameObject character;

    [SerializeField]
    private float speed = 5;

    #endregion Parameters

    #region Methods

    private void Movement()
    {
        Vector3 vector = character.transform.position;
        vector.z = transform.position.z;
        transform.position = Vector3.MoveTowards(transform.position, vector, (vector - transform.position).sqrMagnitude * 5 * Time.deltaTime);
    }

    #endregion Methods

    #region Unity

    private void Update()
    {
        Movement();
    }
    #endregion Unity
}
