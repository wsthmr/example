﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Eminem : MonoBehaviour
{
    #region Parameters

    [SerializeField]
    private float speed = 5;

    #endregion Parameters

    #region State

    private Vector2 direction;

    private SpriteRenderer renderrer;

    #endregion State

    #region Methods

    private void Movement(Vector3 direction)
    {
        transform.position = Vector3.MoveTowards(transform.position, direction, speed * Time.deltaTime);
    }

    private void CheckWalls()
    {
        Collider2D[] collidersDown = Physics2D.OverlapCircleAll(new Vector2(transform.position.x + 1 * direction.x, transform.position.y - 1), 0.2f);
        Collider2D[] collidersForward = Physics2D.OverlapCircleAll(new Vector2(transform.position.x + 1 * direction.x, transform.position.y), 0.2f);
        bool isWall = false;
        foreach (Collider2D el in collidersForward)
        {
            if (el.GetComponent<Block>() != null) isWall = true;
        }

        if (isWall || collidersDown.Length == 0)
        {
            direction *= -1;
            renderrer.flipX = !renderrer.flipX;
        }
    }
    #endregion Methods

    #region Unity
    private void Awake()
    {
        direction = Vector2.right;
        renderrer = GetComponent<SpriteRenderer>();
    }
    private void Update()
    {
        Movement(new Vector2(transform.position.x + direction.x, transform.position.y));
        CheckWalls();
    }
    #endregion Unity
}
